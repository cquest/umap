��    ,      |  ;   �      �     �  %   �  ,   �     "     A     a           �     �     �     �     �     �     �               !     )     C  	   G     Q     X     `  =   ~     �     �     �     �     �     �  2        D  "   `     �  %   �     �     �     �     �     �     �       �     l  �     W	  0   ^	  0   �	  %   �	  "   �	     	
  (   "
     K
     c
     q
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
                  "   %  E   H     �     �  
   �     �     �     �  2   �     !  "   =     `  %   i     �     �     �     �     �     �     �     �                                                   
              (       '      +              ,      	              !   &      "                #       $                  *                 )   %              About Add POIs: markers, lines, polygons... Batch import geostructured data (GEOJson...) Browse %(current_user)s's maps Change marker shape and symbol. Choose the layers of your map Choose the licence for your data Choose your tilelayer Create a map Create your map now! Cured by Delete Details on formats Embed and share your map Feedback GPX GeoJSON Get inspired, browse maps KML Last maps Log in Log out Manage POIs colours and icons Manage map options: display a minimap, locate user on load… Map settings More My maps Not map found. Play with the demo Please choose a provider Properties used: name or title, description, color Properties used: name, desc Properties used: name, description Search Search for maps containing «%(q)s»  Search maps See this map! Sign in Styles options Upload features What can you do? What is uMap uMap let you create maps with <a href="%(osm_url)s" />OpenStreetMap</a> layers in a minute and embed them in your site.<br /> This is a demo instance, you can host your own, it's <a href="%(repo_url)s">open source</a>! Project-Id-Version: 0.2.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-02-24 11:45+0100
PO-Revision-Date: 2013-03-10 20:00+0100
Last-Translator: Francisco DOS SANTOS <f.dos.santos@free.fr>
Language-Team: PT <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
 Acerca Adicionar objetos: marcos, linhas, polígonos... Carregar ficheiros georeferenciados (GEOJson...) Consulta os mapas de %(current_user)s Mudar forma e pictograma do marco. Escolher o mapa de fundo Escolher uma licença para os seus dados Escolha o mapa de fundo Criar um mapa Cria o seu mapa já! Mantido por Apagar Detalhes nos formatos Exportar e partilhar seu mapa Contactar GPX GeoJSON Navega nos mapas existente KML Últimos mapas Identificar-se Sair Modificar cor e ícone dos objetos Gerir diversas opções: mostrar um minimapa, localizar o usuário... Parâmetros Mais Meus mapas Nenhum mapa encontrado. Testar a demo Por favor escolha um provedor Uso propriedade: name ou title, description, color Uso propriedade: name, desc Uso propriedade: name, description Procurar Procurar mapas que incluam «%(q)s»  Procurar mapas Ver este mapa! Criar conta Opções de estilo Carregar objetos O que podes fazer O que é uMap uMap permita criar num instante mapas personalizados com <a href="%(osm_url)s" />OpenStreetMap</a> e mostrar-los no seu site.<br /> Aqui é uma demonstração, podes instalar este software no teu próprio site, é <a href="%(repo_url)s">software livre</a>! 